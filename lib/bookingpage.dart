




import 'dart:io';

import 'package:firebase_app_project/add_profile.dart';
import 'package:firebase_app_project/login.dart';
import 'package:firebase_app_project/profile_model.dart';
import 'package:firebase_app_project/show_profile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'database.dart';
import 'edit_profile.dart';

class Booking extends StatefulWidget {
  const Booking({Key key,  this.title}) : super(key: key);

  final String title;

  @override
  State<Booking> createState() => _BookingState();
}

class _BookingState extends State<Booking> {
  int selectedId;
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('CRM of PSU Booking System',
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 23,
              color: Colors.white70),),
      ),

      body: Center(
        child: FutureBuilder<List<BookingModel>>(
            future: DatabaseHelper.instance.getGroceries(),
            builder: (BuildContext context,
                AsyncSnapshot<List<BookingModel>> snapshot) {
              if (!snapshot.hasData) {
                return Center(child: Text('Loading...'));
              }
              return snapshot.data.isEmpty
              // ? Center(child: Text('No Profiles in List.'))
              //it is the add start
                  ? Center(
                  child: ListView(
                    padding: EdgeInsets.zero,
                    children: <Widget>[
                      TextButton(
                        onPressed: () {
                          //forgot password screen
                        },
                        child: const Text('Adding and Checking Booking of Customer',style: TextStyle(
                            color: Colors.blueGrey,
                            fontSize: 18,
                            fontWeight: FontWeight.bold),),
                      ),
                      Container(
                          height: 47,
                          padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                          child: ElevatedButton(
                            child: const Text('Add Booking',style: TextStyle(
                                color: Colors.white70,
                                fontSize: 21,
                                fontWeight: FontWeight.bold),),
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          AddProfile()));
                            },
                          )
                      ),
                      SizedBox(height: 20,),
                      Container(
                          height: 47,
                          padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                          child: ElevatedButton(
                            child: const Text('Check Booking Info',style: TextStyle(
                                color: Colors.white70,
                                fontSize: 21,
                                fontWeight: FontWeight.bold),),
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          Booking()));
                            },
                          )
                      ),
                     /* Row(
                        children: <Widget>[
                          const Text('Does not have account?',style: TextStyle(
                              color: Colors.blueGrey,
                              fontSize: 21,
                              fontWeight: FontWeight.bold),),
                          TextButton(
                            child: const Text(
                              'Sign In',
                              style: TextStyle(fontSize: 20, color: Colors.lightBlue,fontWeight: FontWeight.bold),
                            ),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => Login()),
                              ).then((value) {
                                //getAllData();
                                setState(() {});
                              });
                            },
                          )
                        ],
                        mainAxisAlignment: MainAxisAlignment.center,
                      ),*/

                    ],
                  )) //it is the adding end
                  : ListView(
                children: snapshot.data.map((grocery) {
                  return Center(
                    child: Card(
                      color: selectedId == grocery.id
                          ? Colors.white70
                          : Colors.white,
                      child: ListTile(
                        title: Text(
                          '${grocery.firstname} ${grocery.lastname} (${grocery.phone})',
                          style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.bold,
                              color: Colors.blueGrey),
                        ),
                        subtitle: Text(
                          '${grocery.email}',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                              color: Colors.blueGrey),
                        ),
                        leading: CircleAvatar(
                            minRadius: 15,
                            maxRadius: 37,
                            backgroundImage: FileImage(
                              File(grocery.image),
                            )),
                        trailing: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            new IconButton(
                              padding: EdgeInsets.all(0),
                              icon: Icon(Icons.edit),
                              iconSize: 25,
                              color: Colors.blueGrey,
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          EditProfile(grocery)),
                                ).then((value) {
                                  setState(() {});
                                });
                              },
                            ),
                            new IconButton(
                              padding: EdgeInsets.all(0),
                              icon: Icon(Icons.cancel),
                              iconSize: 25,
                              color: Colors.blueGrey,
                              onPressed: () {
                                showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      title: new Text(
                                          "Do you want to delete customer booking?",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 23,
                                            color: Colors.blueGrey),),
                                      // content: new Text("Please Confirm"),
                                      actions: [
                                        new TextButton(
                                          onPressed: () {
                                            DatabaseHelper.instance
                                                .remove(grocery.id);
                                            setState(() {
                                              Navigator.of(context).pop();
                                            });
                                          },
                                          child: new Text("Ok"),
                                        ),
                                        Visibility(
                                          visible: true,
                                          child: new TextButton(
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                            child: new Text("Cancel"),
                                          ),
                                        ),
                                      ],
                                    );
                                  },
                                );
                              },
                            ),
                          ],
                        ),
                        onTap: () {
                          var profileid = grocery.id;
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      ShowProfile(id: profileid)));

                          setState(() {
                            print(grocery.image);
                            if (selectedId == null) {
                              //firstname.text = grocery.firstname;
                              selectedId = grocery.id;
                            } else {
                              // textController.text = '';
                              selectedId = null;
                            }
                          }

                          );
                        },

                        onLongPress: () {
                          setState(() {
                            DatabaseHelper.instance.remove(grocery.id);
                          });
                        },
                      ),
                    ),
                  );
                }).toList(),
              );
            }),
      ),
    );
  }
}
