import 'package:firebase_app_project/signup.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:firebase_app_project/authentication.dart';
import 'package:firebase_app_project/login.dart';
import 'package:flutter_image_slideshow/flutter_image_slideshow.dart';

import 'aboutUs.dart';
import 'customers.dart';
import 'developer.dart';
import 'drawerbody.dart';
import 'drawerheader.dart';
import 'homepage.dart';

class Home extends StatelessWidget {
  static const String routeName = '/home';
  FirebaseAuth _auth = FirebaseAuth.instance;
  get user => _auth.currentUser;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white70,
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.logout,
              color: Colors.white54,
              size: 30,
            ),
            onPressed: () {
              AuthenticationHelper()
                  .signOut()
                  .then((_) => Navigator.pushReplacement(
                context,
                MaterialPageRoute(builder: (contex) => Login()),
              ));
            },
          )
        ],
        systemOverlayStyle: SystemUiOverlayStyle(statusBarColor: Colors.black54),
        title: Text(
          "Administration Control Page",
          style: TextStyle(fontSize: 22, color: Colors.white54, fontWeight:  FontWeight.bold),
        ),
      ),
      drawer: Drawer(
        backgroundColor: Colors.blueGrey[200],
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            createDrawerHeader(),
            createDrawerBodyItem(
              icon: Icons.home,
              text: 'Customer Page',
              onTap: () =>
              // Navigator.pushReplacementNamed(context, pageRoutes.home),
              Navigator.push(context,
                  MaterialPageRoute(
                      builder: (context) => HomePage())),
            ),
            // createDrawerHeader(),


            createDrawerBodyItem(
              icon: Icons.account_circle,
              text: 'Developer Page',
              onTap: () =>
              //Navigator.pushReplacementNamed(context, pageRoutes.profile),
              Navigator.push(context,
                  MaterialPageRoute(
                      builder: (context) => Developer())),
            ),

            createDrawerBodyItem(
              //icon: Icons.contact_phone,
              icon: Icons.emoji_objects,
              text: 'Administration Page',
              onTap: () =>
              // Navigator.pushReplacementNamed(context, pageRoutes.event),
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => Home())),
            ),
            createDrawerBodyItem(
              //icon: Icons.contact_phone,
              icon: Icons.book,
              text: 'About System Page',
              onTap: () =>
              // Navigator.pushReplacementNamed(context, pageRoutes.event),
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => aboutUs())),
            ),
            createDrawerBodyItem(
              //icon: Icons.contact_phone,
              icon: Icons.article,
              text: 'Login Page',
              onTap: () =>
              // Navigator.pushReplacementNamed(context, pageRoutes.event),
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => Login())),
            ),
          ],
        ),
      ),//set app bar
      body: Center(
        /*child: Container(
          child: Text(
            'Welcome ' + user.email,
            style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold, color: Colors.blueGrey),
          ),
        ),*/
        child: Column(
          children: [
            SizedBox(height: 30),
            Padding(
              padding: const EdgeInsets.all(30),
              child: Text(
              'Welcome to the System:',
                style: TextStyle(
                    fontSize: 27,
                    fontWeight: FontWeight.bold,
                    color: Colors.black38),
              ),
            ),
          /*  Divider(
              thickness: 0.8,
              color: Colors.blueGrey,
            ),*/
            Padding(
              padding: const EdgeInsets.all(30),
              child: Text(
                'Admin: '+user.email,
                style: TextStyle(
                    fontSize: 19,
                    fontWeight: FontWeight.bold,
                    color: Colors.blueGrey),
              ),
            ),
            Divider(
              thickness: 0.8,
              color: Colors.blueGrey,
            ),
            ImageSlideshow(
              width: double.infinity,

              /// Height of the [ImageSlideshow].
              height: 200,

              /// The page to show when first creating the [ImageSlideshow].
              initialPage: 0,

              /// The color to paint the indicator.
              indicatorColor: Colors.blue,

              /// The color to paint behind th indicator.
              indicatorBackgroundColor: Colors.grey,

              /// The widgets to display in the [ImageSlideshow].
              /// Add the sample image file into the images folder
              children: [
                Image.asset(
                  'assets/images/vila.jpg',
                  fit: BoxFit.cover,
                ),
                Image.asset(
                  'assets/images/pakbong.jpg',
                  fit: BoxFit.cover,
                ),
                Image.asset(
                  'assets/images/nasria.jpg',
                  fit: BoxFit.cover,
                ),
                Image.asset(
                  'assets/images/banana.jpg',
                  fit: BoxFit.cover,
                ),
              ],

              /// Called whenever the page in the center of the viewport changes.
              onPageChanged: (value) {
                print('Page changed: $value');
              },

              /// Auto scroll interval.
              /// Do not auto scroll with null or 0.
              autoPlayInterval: 3000,

              /// Loops back to first slide.
              isLoop: true,
            ),
            Divider(
              thickness: 0.8,
              color: Colors.blueGrey,
            ),
            SizedBox(height: 30),
            ElevatedButton(
              onPressed: () {
                //แทรกโค้ดให้คลิกแล้วกลับไปหน้าแรก
                //
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Signup()));
              },
              child: Text("Register New Admin", style: TextStyle(color: Colors.white54, fontWeight: FontWeight.bold,
              fontSize: 21),),
              style: ElevatedButton.styleFrom(primary: Colors.blueGrey),
            ),
          ],
        ),
      ),
    );
  }
}
