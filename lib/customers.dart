import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_app_project/bookingpage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:flutter_slidable/flutter_slidable.dart';

import 'aboutUs.dart';
import 'authentication.dart';
import 'developer.dart';
import 'drawerbody.dart';
import 'drawerheader.dart';
import 'home.dart';
import 'login.dart';

class Customer extends StatefulWidget {
  static const String routeName = '/Customer';
  const Customer({Key key}) : super(key: key);

  @override
  State<Customer> createState() => _CustomerState();
}


class _CustomerState extends State<Customer> {
  List dataList = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black26,
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.logout,
              color: Colors.white54,
              size: 30,
            ),
            onPressed: () {
              AuthenticationHelper()
                  .signOut()
                  .then((_) => Navigator.pushReplacement(
                context,
                MaterialPageRoute(builder: (contex) => Login()),
              ));
            },
          )
        ],
        systemOverlayStyle: SystemUiOverlayStyle(statusBarColor: Colors.black54),
        title: const Text('Customers Detail Page',
            style: TextStyle(color: Colors.white54, fontSize: 22, fontWeight: FontWeight.bold)),
      ),
     /* drawer: Drawer(
        backgroundColor: Colors.blueGrey[200],
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            createDrawerHeader(),
            createDrawerBodyItem(
              icon: Icons.home,
              text: 'Customer Page',
              onTap: () =>
              // Navigator.pushReplacementNamed(context, pageRoutes.home),
              Navigator.push(context,
                  MaterialPageRoute(
                      builder: (context) => Customer())),
            ),
            // createDrawerHeader(),


            createDrawerBodyItem(
              icon: Icons.account_circle,
              text: 'Developer Page',
              onTap: () =>
              //Navigator.pushReplacementNamed(context, pageRoutes.profile),
              Navigator.push(context,
                  MaterialPageRoute(
                      builder: (context) => Developer())),
            ),

            createDrawerBodyItem(
              //icon: Icons.contact_phone,
              icon: Icons.emoji_objects,
              text: 'Administration Page',
              onTap: () =>
              // Navigator.pushReplacementNamed(context, pageRoutes.event),
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => Home())),
            ),
            createDrawerBodyItem(
              //icon: Icons.contact_phone,
              icon: Icons.book,
              text: 'About System Page',
              onTap: () =>
              // Navigator.pushReplacementNamed(context, pageRoutes.event),
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => aboutUs())),
            ),
            createDrawerBodyItem(
              //icon: Icons.contact_phone,
              icon: Icons.article,
              text: 'Login Page',
              onTap: () =>
              // Navigator.pushReplacementNamed(context, pageRoutes.event),
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => Login())),
            ),
          ],
        ),
      ),//set app bar*/
      body: FutureBuilder(
        future: FireStoreDataBase().getData(),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return const Text(
              "Something went wrong",
            );
          }
          if (snapshot.connectionState == ConnectionState.done) {
            dataList = snapshot.data as List;
            return buildItems(dataList);
          }
          return const Center(child: CircularProgressIndicator());
        },
      ),
    );
  }

  Widget buildItems(dataList) =>
      ListView.separated(
          padding: const EdgeInsets.all(8),
          itemCount: dataList.length,
          separatorBuilder: (BuildContext context, int index) =>
          const Divider(thickness: 2,
              color: Colors.blueGrey),
          itemBuilder: (BuildContext context, int index) {
            /* return ListTile(
          title: Text(
            dataList[index]["fullname".toString()],
          ),
          subtitle:  Text(dataList[index]["phonenumber"].toString()),
          trailing: Text(
            dataList[index]["gender"],
          ),
        );*/
            return Center(
              child: Column(
                children: [
                  SizedBox(height: 30),
                  Text(dataList[index]["fullname".toString()], style: TextStyle(
                      fontSize: 21,
                      fontWeight: FontWeight.bold,
                      color: Colors.blueGrey),
                  ),
                  Text(dataList[index]["gender".toString()], style: TextStyle(
                      fontSize: 21,
                      fontWeight: FontWeight.bold,
                      color: Colors.blueGrey),
                  ),
                  Text(dataList[index]["age"].toString(), style: TextStyle(
                      fontSize: 21,
                      fontWeight: FontWeight.bold,
                      color: Colors.blueGrey),
                  ),
                  Text(
                    dataList[index]["phonenumber".toString()], style: TextStyle(
                      fontSize: 21,
                      fontWeight: FontWeight.bold,
                      color: Colors.blueGrey),
                  ),
                  Text(dataList[index]["province".toString()], style: TextStyle(
                      fontSize: 21,
                      fontWeight: FontWeight.bold,
                      color: Colors.blueGrey),
                  ),
                  SizedBox(height: 30),
                  ElevatedButton(
                    onPressed: () {
                      //แทรกโค้ดให้คลิกแล้วกลับไปหน้าแรก
                       // Navigator.pop(context);
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  Booking()));
                    },
                    child: Text("Make Booking", style: TextStyle(color: Colors.white54,),),
                    style: ElevatedButton.styleFrom(primary: Colors.blueGrey),
                  ),
                  SizedBox(height: 30),


                ],
              ),

            );
          }
      );
}

class FireStoreDataBase {
  List customerList = [];
  final CollectionReference collectionRef =
  FirebaseFirestore.instance.collection("customers");

  Future getData() async {
    try {
      //to get data from a single/particular document alone.
      // var temp = await collectionRef.doc("<your document ID here>").get();

      // to get data from all documents sequentially
      await collectionRef.get().then((querySnapshot) {
        for (var result in querySnapshot.docs) {
          customerList.add(result.data());
        }
      });

      return customerList;
    } catch (e) {
      debugPrint("Error - $e");
      return null;
    }
  }
}