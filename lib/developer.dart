import 'package:firebase_app_project/home.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_image_slideshow/flutter_image_slideshow.dart';
import 'aboutUs.dart';
import 'authentication.dart';
import 'customers.dart';
import 'drawerbody.dart';
import 'drawerheader.dart';
import 'home.dart';
import 'homepage.dart';
import 'login.dart';

class Developer extends StatelessWidget {
  static const String routeName = '/developer';
  @override
  Widget build(BuildContext context) {
    var pageRoutes;
    return new Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.logout,
              color: Colors.white54,
              size: 30,
            ),
            onPressed: () {
              AuthenticationHelper()
                  .signOut()
                  .then((_) => Navigator.pushReplacement(
                context,
                MaterialPageRoute(builder: (contex) => Login()),
              ));
            },
          )
        ],
        systemOverlayStyle: SystemUiOverlayStyle(statusBarColor: Colors.black54),
        title: Text("About Developer Page", style: TextStyle(color: Colors.white54, fontSize: 23, fontWeight: FontWeight.bold),),
      ),
      drawer: Drawer(
        backgroundColor: Colors.blueGrey[200],
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            createDrawerHeader(),
            createDrawerBodyItem(
              icon: Icons.home,
              text: 'Customer Page',
              onTap: () =>
              // Navigator.pushReplacementNamed(context, pageRoutes.home),
              Navigator.push(context,
                  MaterialPageRoute(
                      builder: (context) => HomePage())),
            ),
            // createDrawerHeader(),


            createDrawerBodyItem(
              icon: Icons.account_circle,
              text: 'Developer Page',
              onTap: () =>
              //Navigator.pushReplacementNamed(context, pageRoutes.profile),
              Navigator.push(context,
                  MaterialPageRoute(
                      builder: (context) => Developer())),
            ),

            createDrawerBodyItem(
              //icon: Icons.contact_phone,
              icon: Icons.emoji_objects,
              text: 'Administration Page',
              onTap: () =>
              // Navigator.pushReplacementNamed(context, pageRoutes.event),
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => Home())),
            ),
            createDrawerBodyItem(
              //icon: Icons.contact_phone,
              icon: Icons.book,
              text: 'About System Page',
              onTap: () =>
              // Navigator.pushReplacementNamed(context, pageRoutes.event),
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => aboutUs())),
            ),
            createDrawerBodyItem(
              //icon: Icons.contact_phone,
              icon: Icons.article,
              text: 'Login Page',
              onTap: () =>
              // Navigator.pushReplacementNamed(context, pageRoutes.event),
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => Login())),
            ),
          ],
        ),
      ),//set app bar
      //drawer: navigationDrawer(),
      //  body: Center(child: Text("This is profile page"))
      body: Container(
        padding: EdgeInsets.only(
          top: 20,
          left: 20,
          right: 20,
          bottom: 20,
        ),
        color: Colors.grey[200],
        constraints: BoxConstraints.expand(),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Divider(
                thickness: 0.8,
                color: Colors.blueGrey,
              ),
              ImageSlideshow(
                width: double.infinity,

                /// Height of the [ImageSlideshow].
                height: 200,

                /// The page to show when first creating the [ImageSlideshow].
                initialPage: 0,

                /// The color to paint the indicator.
                indicatorColor: Colors.blue,

                /// The color to paint behind th indicator.
                indicatorBackgroundColor: Colors.grey,

                /// The widgets to display in the [ImageSlideshow].
                /// Add the sample image file into the images folder
                children: [
                  Image.asset(
                    'assets/images/logo1.jpg',
                    fit: BoxFit.cover,
                  ),
                  Image.asset(
                    'assets/images/logo2.jpg',
                    fit: BoxFit.cover,
                  ),
                  Image.asset(
                    'assets/images/logo3.jpg',
                    fit: BoxFit.cover,
                  ),
                ],

                /// Called whenever the page in the center of the viewport changes.
                onPageChanged: (value) {
                  print('Page changed: $value');
                },

                /// Auto scroll interval.
                /// Do not auto scroll with null or 0.
                autoPlayInterval: 3000,

                /// Loops back to first slide.
                isLoop: true,
              ),
              SizedBox(height: 15),
              Divider(
                thickness: 0.8,
                color: Colors.blueGrey,
              ),
              Row(
                children: [
                  CircleAvatar(
                    radius: 38,
                    backgroundImage: AssetImage('assets/images/vila.jpg'),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      left: 15,
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Mr. Vila Leng',
                          style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.black54),
                        ),
                        Text(
                          'Software Developer',
                          style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                            color: Colors.black45,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Icon(
                    Icons.flutter_dash,
                    size: 58,
                    color: Colors.blueGrey,
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(20),
                child: Text(
                  'My name is Mr. Vila Leng, you can call me Win, my ID is 6250110030. I am a junior student at Prince of Songkla University'
                  ', Trang Campus. My Major is Information and Computer Management.'
                  'I will be a skillful Software Developer.',
                  style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      color: Colors.blueGrey),
                ),
              ),
              Divider(
                thickness: 0.8,
                color: Colors.blueGrey,
              ),
              Row(
                children: [
                  CircleAvatar(
                    radius: 38,
                    backgroundImage: AssetImage('assets/images/pakbong.jpg'),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      left: 15,
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Ms.Wannaporn Thongkaw',
                          style: TextStyle(
                              fontSize: 13,
                              fontWeight: FontWeight.bold,
                              color: Colors.black54),
                        ),
                        Text(
                          'Computer Programmer',
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.black45,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Icon(
                    Icons.computer_sharp,
                    size: 58,
                    color: Colors.blueGrey,
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(20),
                child: Text(
                  'My name is Ms.Wannaporn Thongkaw , you can call me Pakbong, my ID is 6250110012. I am a junior student at Prince of Songkla University'
                  ', Trang Campus. My Major is Information and Computer Management.'
                  'I want to be a Computer Programmer.',
                  style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      color: Colors.blueGrey),
                ),
              ),
              Divider(
                thickness: 0.8,
                color: Colors.blueGrey,
              ),
              Row(
                children: [
                  CircleAvatar(
                    radius: 38,
                    backgroundImage: AssetImage('assets/images/nasria.jpg'),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      left: 15,
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Ms.Woralak Samart',
                          style: TextStyle(
                              fontSize: 13,
                              fontWeight: FontWeight.bold,
                              color: Colors.black54),
                        ),
                        Text(
                          'Computer Programmer',
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.black45,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Icon(
                    Icons.computer_sharp,
                    size: 58,
                    color: Colors.blueGrey,
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(20),
                child: Text(
                  'My name is Ms.Woralak Samart , you can call me Nasria, my ID is 6250110028. I am a junior student at Prince of Songkla University'
                  ', Trang Campus. My Major is Information and Computer Management.'
                  'I want to be a Computer Programmer.',
                  style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      color: Colors.blueGrey),
                ),
              ),
              Divider(
                thickness: 0.8,
                color: Colors.blueGrey,
              ),
              Row(
                children: [
                  CircleAvatar(
                    radius: 38,
                    backgroundImage: AssetImage('assets/images/banana.jpg'),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      left: 15,
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Mr.Apisit Madhadam',
                          style: TextStyle(
                              fontSize: 13,
                              fontWeight: FontWeight.bold,
                              color: Colors.black54),
                        ),
                        Text(
                          'Computer Programmer',
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.black45,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Icon(
                    Icons.computer_sharp,
                    size: 58,
                    color: Colors.blueGrey,
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(20),
                child: Text(
                  'My name is Mr.Apisit Madhadam , you can call me Klouy Horm, my ID is 6250110017. I am a junior student at Prince of Songkla University'
                  ', Trang Campus. My Major is Information and Computer Management.'
                  'I want to be a Computer Programmer.',
                  style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      color: Colors.blueGrey),
                ),
              ),
              Divider(
                thickness: 0.8,
                color: Colors.blueGrey,
              ),
              ElevatedButton.icon(
                onPressed: () {
                  //แทรกโค้ดให้คลิกแล้วกลับไปหน้าแรก
                  //
                  Navigator.pop(context);
                  /*Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));*/
                },
                icon: Icon(
                  Icons.arrow_back_ios,
                  color: Colors.white,
                ),
                label: Text("Go Back"),
                style: ElevatedButton.styleFrom(primary: Colors.blueGrey),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
