


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'drawerbody.dart';
import 'drawerheader.dart';


class navigationDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var pageRoutes;
    return Drawer(
      backgroundColor: Colors.blueGrey[200],
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          createDrawerHeader(),
          createDrawerBodyItem(
            icon: Icons.home,
            text: 'Home Page',
            onTap: () =>
                Navigator.pushReplacementNamed(context, pageRoutes.customer),
          ),
          // createDrawerHeader(),


          createDrawerBodyItem(
            icon: Icons.account_circle,
            text: 'Profile Page',
            onTap: () =>
                Navigator.pushReplacementNamed(context, pageRoutes.developer),
          ),

          createDrawerBodyItem(
            //icon: Icons.contact_phone,
            icon: Icons.exit_to_app,
            text: 'Log Out',
            onTap: () =>
                Navigator.pushReplacementNamed(context, pageRoutes.home),
          ),
          createDrawerBodyItem(
            //icon: Icons.contact_phone,
            icon: Icons.exit_to_app,
            text: 'News',
            onTap: () =>
                Navigator.pushReplacementNamed(context, pageRoutes.login),
          ),
        ],
      ),
    );
  }
}
