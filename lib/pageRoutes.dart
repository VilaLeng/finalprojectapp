



import 'package:firebase_app_project/customers.dart';
import 'package:firebase_app_project/developer.dart';
import 'package:firebase_app_project/home.dart';
import 'package:firebase_app_project/homepage.dart';
import 'package:firebase_app_project/login.dart';
import 'package:firebase_app_project/aboutUs.dart';

class pageRoutes {
  static const String customer = Customer.routeName;

  static const String developer = Developer.routeName;
  static const String home = Home.routeName;
  static const String login = Login.routeName;
  static const String about = aboutUs.routeName;
  static const String homepage =HomePage.routeName;

}
