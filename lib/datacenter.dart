import 'package:flutter/material.dart';
import 'dart:math';
import 'package:fl_chart/fl_chart.dart';

import 'authentication.dart';
import 'login.dart';

class DataCenter extends StatelessWidget {
  DataCenter({Key key}) : super(key: key);

  // Generate some dummy data for the cahrt
  // This will be used to draw the red line
  final List<FlSpot> dummyData1 = List.generate(8, (index) {
    return FlSpot(index.toDouble(), index * Random().nextDouble());
  });

  // This will be used to draw the orange line
  final List<FlSpot> dummyData2 = List.generate(8, (index) {
    return FlSpot(index.toDouble(), index * Random().nextDouble());
  });

  // This will be used to draw the blue line
  final List<FlSpot> dummyData3 = List.generate(8, (index) {
    return FlSpot(index.toDouble(), index * Random().nextDouble());
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.blueGrey,
          actions: <Widget>[
          IconButton(
          icon: Icon(
          Icons.logout,
          color: Colors.white54,
          size: 30,
      ),
      onPressed: () {
        AuthenticationHelper()
            .signOut()
            .then((_) => Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (contex) => Login()),
        ));
      },
    )
    ],
    title: const Text(
    'Customers List Management',
    style: TextStyle(
    fontWeight: FontWeight.bold, fontSize: 19, color: Colors.white54),
    ),
    ),
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.all(20),
          width: double.infinity,
          child: LineChart(
            LineChartData(
              borderData: FlBorderData(show: false),
              lineBarsData: [
                // The red line
                LineChartBarData(
                  spots: dummyData1,
                  isCurved: true,
                  barWidth: 3,
                  colors: [
                    Colors.red,
                  ],
                ),
                // The orange line
                LineChartBarData(
                  spots: dummyData2,
                  isCurved: true,
                  barWidth: 3,
                  colors: [
                    Colors.orange,
                  ],
                ),
                // The blue line
                LineChartBarData(
                  spots: dummyData3,
                  isCurved: false,
                  barWidth: 3,
                  colors: [
                    Colors.blue,
                  ],
                ),

              ],
            ),

          ),
        ),
      ),
    );
  }
}
