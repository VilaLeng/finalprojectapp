
class BookingModel {
  int id;
  String firstname;
  String lastname;
  String email;
  String phone;
  String image;

  BookingModel({
    this.id,
     this.firstname,
     this.lastname,
     this.email,
     this.phone,
     this.image,

  });

  factory BookingModel.fromMap(Map<String, dynamic> json) =>
      new BookingModel(
        id: json['id'],
        firstname: json['firstname'],
        lastname: json['lastname'],
        email: json['email'],
        phone: json['phone'],
        image: json['image'],
      );

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'firstname': firstname,
      'lastname': lastname,
      'email': email,
      'phone': phone,
      'image': image,
    };
  }
}